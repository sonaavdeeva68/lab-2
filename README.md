# Лабораторна робота №2. Вступ до програмування. Основи debug процесу

## Мета
Установка середовища для подальшої роботи з предмету "Програмування". 

## 1 Вимоги

### 1.1 Розробник
Інформація про розробника:
- Авдєєва Софія Олександрівна 
- Група КІТ 121Б

### 1.2 Загальне завдання
- Установити середовище для подальшої роботи з предмету "Програмування"
- Зробити тест дебаг процесу, роблячи зміни у заданому репозиторії

### 1.3 Задача
- Зареєструватися на gitlab, github
- Склонуватися з створеного репозиторія
- Внести в нього зміни
- Завантажити зміни на видалений сервер

## Процес виконання
Створені акаунти на gitlab та github:
- Gitlab https://gitlab.com/sonaavdeeva68
- Github https://github.com/sonaavdeeva

> - Я створила приватний репозиторій під назвою "lab02", щоб потім завантажити вже виконану лабораторну роботу;
> - За допомогою команди `git init` створила репозиторій на локальному рівні на своему компʼютері;
> - За допомогою команди `git remote add origin https://gitlab.com/sonaavdeeva68/lab02.git` підключилася до видаленого репозиторію, щоб після виконання роботи відправити всі файли в інтернет;
> - Створила піддерикторію lab02 та копіювала туди вказані файли з репозиторія `https://github.com/davydov-vyacheslav/sample_project.git`
> - Зафіксувала зміни у свій репозіторій за допомогою команд `git add *`   та `git commit -m "Initial copy of sam-
ple_project"`
> - Відкрила файл `lib.h` та додала у масив `animal_type` новий елемент - Людину (`HUMAN`).
> - У файл `lib.c` додала до оператора `switch(type)` код `case HUMAN: result = "Людина"; break;`
> - Скомпілювала проект (`make`), перевірила внесені зміни, переконалася у правильній роботі программи.
> - Внесла зміни у свій репозиторій за допомогою команди `git push`

## Висновок
На цій лабораторній работі я навчилася використовувати компілятор `gcc` та утиліту для дебагу `make`. Також, додавши до проекту зміни, я ознайомилася зі структурую програм у мові C.


